package com.konorti.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ProductMain {

	public static void main(String[] args) {
		// 1. Instantiate the Spring IoC container
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		// 2. Get the bean information from Spring IoC
		Product product = applicationContext.getBean("product1",Product.class);
		// 3. Use the bean
		System.out.println(product);
	}
}
