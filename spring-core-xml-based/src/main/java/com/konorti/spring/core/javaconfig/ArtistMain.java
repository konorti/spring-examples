package com.konorti.spring.core.javaconfig;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ArtistMain {

	public static void main(String[] args) {
		BeanFactory beanFactory = new AnnotationConfigApplicationContext(ArtistConfiguration.class);
		Artist artist = beanFactory.getBean("artist", Artist.class);
		System.out.println(artist);
	}
}
