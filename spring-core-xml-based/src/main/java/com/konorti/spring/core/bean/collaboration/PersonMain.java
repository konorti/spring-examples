package com.konorti.spring.core.bean.collaboration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonMain {

	public static void main(String args[]) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext-beancolab1.xml");
		Person p = applicationContext.getBean("person", Person.class);
		System.out.println(p);
	}
}
