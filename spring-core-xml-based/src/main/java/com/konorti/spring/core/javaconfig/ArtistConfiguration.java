package com.konorti.spring.core.javaconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ArtistConfiguration {
	
	@Bean
	public Artist artist() {
		return new Artist("John", "Doe");
	}
}
