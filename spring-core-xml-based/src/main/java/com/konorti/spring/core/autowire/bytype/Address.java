package com.konorti.spring.core.autowire.bytype;

public class Address {

	private String unitName;
	private String street;
	private String area;
	private String pin;
	
	public String getUnitName() {
		return unitName;
	}
	public String getStreet() {
		return street;
	}
	public String getArea() {
		return area;
	}
	public String getPin() {
		return pin;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	@Override
	public String toString() {
		return "Address [unitName=" + unitName + ", street=" + street + ", area=" + area + ", pin=" + pin + "]";
	}
}
