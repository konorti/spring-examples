package com.konorti.spring.core.xmlconfig.setter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.konorti.spring.core.xmlconfig.constructor.Person;

public class ArtistMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext-setter.xml");
		Artist a = applicationContext.getBean("artist", Artist.class);
		System.out.println(a);
	}
}
