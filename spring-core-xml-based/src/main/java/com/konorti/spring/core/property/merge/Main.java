package com.konorti.spring.core.property.merge;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("applicationContext-collection-merge.xml");
		EmailDetails emailDetails = applicationContext.getBean("emailDetails", EmailDetails.class);
		System.out.println(emailDetails);
	}

}
