package com.konorti.spring.core.beanimport;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext-artist.xml");
		Artist artist = applicationContext.getBean("artist", Artist.class);
		System.out.println(artist);
	}
}
