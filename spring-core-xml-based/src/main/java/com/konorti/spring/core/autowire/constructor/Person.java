package com.konorti.spring.core.autowire.constructor;

import java.beans.ConstructorProperties;

public class Person {

	private int id;
	private String firstName;
	private String lastName;
	private Address address;

	@ConstructorProperties({"id", "firstName", "lastName", "address"})
	public Person(int id, String firstName, String lastName, Address address) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address
				+ "]";
	}
}
