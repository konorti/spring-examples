package com.konorti.spring.core.beanimport;

public class Instrument {

	private String id;
	private String name;
	
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Instrument [id=" + id + ", name=" + name + "]";
	}
}
