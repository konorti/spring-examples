package com.konorti.spring.core.autowire.bytype;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonMain {

	public static void main(String args[]) {
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("applicationContext-autowire-bytype.xml");
		Person p = applicationContext.getBean("person", Person.class);
		System.out.println(p);
	}
}
