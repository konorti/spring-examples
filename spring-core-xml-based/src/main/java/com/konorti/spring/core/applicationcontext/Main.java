package com.konorti.spring.core.applicationcontext;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("applicationContext-album.xml", "applicationContext-song.xml");
		Album album = applicationContext.getBean("album", Album.class);
		System.out.println(album);
		Song song = applicationContext.getBean("song", Song.class);
		System.out.println(song);
	}
}
