package com.konorti.spring.core.autowire.base;

import org.springframework.beans.factory.annotation.Autowired;

public class UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save() {
        userRepository.save();
    }
}
