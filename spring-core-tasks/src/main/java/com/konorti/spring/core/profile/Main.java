package com.konorti.spring.core.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext();
        applicationContext.getEnvironment().setDefaultProfiles("Dev");
        applicationContext.scan("com.konorti.spring.core.profile");
        applicationContext.refresh();
        applicationContext
                .getBean("databaseConfigurationHandler", DatabaseConfigurationHandler.class)
                .print();
    }
}
