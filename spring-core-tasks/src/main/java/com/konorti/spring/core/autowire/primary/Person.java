package com.konorti.spring.core.autowire.primary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Person {

    @Autowired
    @Qualifier("car")
    Vehicle vehicle;

    public void driveVehicle() {
        vehicle.drive();
    }
}
