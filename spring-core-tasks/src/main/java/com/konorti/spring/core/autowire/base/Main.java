package com.konorti.spring.core.autowire.base;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(UserConfiguration.class);
        UserService userService = applicationContext.getBean(UserService.class);
        userService.save();
    }
}
