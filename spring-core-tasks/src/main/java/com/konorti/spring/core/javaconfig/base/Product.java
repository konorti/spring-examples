package com.konorti.spring.core.javaconfig.base;

public class Product {
    private String productId;
    private String procuctDesc;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProcuctDesc() {
        return procuctDesc;
    }

    public void setProcuctDesc(String procuctDesc) {
        this.procuctDesc = procuctDesc;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId='" + productId + '\'' +
                ", procuctDesc='" + procuctDesc + '\'' +
                '}';
    }
}
