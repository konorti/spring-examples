package com.konorti.spring.core.factory.staticfactory;

public class DBReader implements Reader{

    @Override
    public void read() {
        System.out.println("DBreader reads.");
    }
}
