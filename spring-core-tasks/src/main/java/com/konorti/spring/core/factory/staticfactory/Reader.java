package com.konorti.spring.core.factory.staticfactory;

public interface Reader {

    void read();
}
