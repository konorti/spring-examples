package com.konorti.spring.core.postprocessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.konorti.spring.core.postprocessor")
public class BeanConfiguration {

    @Bean("Create")
    public Action action1() {
        return new Action("1", "Create");
    }

    @Bean("Update")
    public Action action2() {
        return new Action("2", "Update");
    }
}
