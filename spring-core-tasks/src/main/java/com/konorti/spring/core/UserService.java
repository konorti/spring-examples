package com.konorti.spring.core;

/**
 * Provide CRUD support to the users
 */
public class UserService {
    private UserRepository userRepository;

    public void save(User user) {
        userRepository.save(user);
    }

    public User findById(long userId) {
        return userRepository.findById(userId);
    }

    public void update(User user) {
        userRepository.update(user);
    }

    public void delete(long userId) {
        userRepository.delete(userId);
    }

}
