package com.konorti.spring.core.depends.on;

public class Initialization {

    public void print() {
        System.out.println(this.getClass().getSimpleName());
    }
}
