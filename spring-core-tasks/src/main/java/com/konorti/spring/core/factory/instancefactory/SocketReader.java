package com.konorti.spring.core.factory.instancefactory;

public class SocketReader implements Reader {

    @Override
    public void read() {
        System.out.println("Socket reader reads.");
    }
}
