package com.konorti.spring.core.autowire.primary;

public abstract class Vehicle {

    public abstract void drive();
}
