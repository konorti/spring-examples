package com.konorti.spring.core.autowire.primary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VehicleConfiguration {

    @Bean
    public Vehicle bike() {
        return new Bike();
    }

    @Bean
    public Vehicle car() {
        return new Car();
    }

    @Bean
    public Person person() {
        return new Person();
    }
}
