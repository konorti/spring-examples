package com.konorti.spring.core.factory.instancefactory;

public class DBReader implements Reader {

    @Override
    public void read() {
        System.out.println("DBreader reads.");
    }
}
