package com.konorti.spring.core.autowire.primary;

public class Bike extends Vehicle {

    @Override
    public void drive() {
        System.out.println("Driving bike");
    }
}
